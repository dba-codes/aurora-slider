(function($) {

	$.fn.auroraSlider = function(options) {
		var settings = {
			itemClass: '.as-item',
			startItem: 2,
			infinite: true,
			arrows: false,
			dots: false,
			speed: 1000,
			autoplay: false,
			autoplaySpeed: 6000,
			animation: true,
			easing: 'cubic-bezier(0.785, 0.135, 0.15, 0.86)',
		};

		return this.each(function() {
			//extend the default settings
			if (options) {
				$.extend( settings, options);
			}


			//define elements
			var stage   = $('<div class="as-stage"></div>');
			var track   = $('<div class="as-track"></div>');
			var prev   = $('<div id="as-prev" class="as-trigger"></div>');
			var next   = $('<div id="as-next" class="as-trigger"></div>');

			//define he items and copies
			var	items   = $(this).find(settings.itemClass);
			var	len     = items.length;
			var	first   = items.filter(':first');
			var	last    = items.filter(':last');
			var current = settings.startItem;


			//render stage
			$(this).append(stage);
			//render track and buttons in stage
			$(stage)
				.append(track)
				.append(prev)
				.append(next)
			;

			//window data
			var windowW = track.outerWidth();

			//render and design the track
			$(track)
				.append(items)
				.css({
					left: Math.floor(-windowW * settings.startItem - 1)
				})
			;

			//clone the first and last item
			first.before(last.clone(true)).css('width', windowW);
			last.after(first.clone(true)).css('width', windowW);

			//set item width to track with
			$(items).each(function() {
				$(this).css('width', windowW);
			});

			//get the trigger buttons
			var trigger = $('.as-trigger');

			trigger.each(function() {
				$(this).on('click', function () {
					var cycle, delta;

					if (track.is(':not(:animated)')) {

						cycle = false;
						delta = (this.id === "as-prev") ? -1 : 1;
						/* in the example buttons have id "prev" or "next" */

						track.animate({left: "+=" + (-windowW * delta)}, function () {

							current += delta;

							/**
							 * we're cycling the slider when the the value of "current"
							 * variable (after increment/decrement) is 0 or when it exceeds
							 * the initial gallery length
							 */
							cycle = (current === 0 || current > len);

							if (cycle) {
								/* we switched from image 1 to 4-cloned or
								   from image 4 to 1-cloned */
								current = (current === 0) ? len : 1;
								track.css({left: -windowW * current});
							}
						});
					}
				});
			});
		});
	};
})(jQuery);